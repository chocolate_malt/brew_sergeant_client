// The exported functions here return Promises that are mocked up responses.
// todo: get actual results from server
import { Equipment } from "./models"


async function delayedResponse(response, delay=1000) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(response), delay)
  });
}

export async function getEquipmentItems() {
  let equipmentItems = [];
  equipmentItems.push(
    new Equipment("66L Pot", "66", "Stainless Steel", "2019-03-05")
  );
  equipmentItems.push(
    new Equipment("50L Mash Tun", "50", "Stainless Steel", "2019-03-05")
  );
  return delayedResponse(equipmentItems);
}
