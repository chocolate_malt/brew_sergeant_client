
export class Equipment {
  constructor(name, size, material, created) {
    this.name = name;
    this.size = size;
    this.material = material;
    this.created = created;
  }
}
