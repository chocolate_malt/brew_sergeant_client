import {Box, Button as GrommetButton, Table, TableHeader, TableRow, TableCell, TableBody} from "grommet/es6";
import {Link, Redirect, Route, Switch} from "react-router-dom";
import React, { Component } from "react"
import { getEquipmentItems } from "./api/interface";


class EquipmentTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      equipmentItems: [],
      itemsLoaded: false
    };
    this.getItems();
  }

  async getItems() {
    let items = await getEquipmentItems();
    this.setState({
      equipmentItems: items,
      itemsLoaded: true
    });
  }


  render () {
    // const { equipmentItems, itemsLoaded } = this.state;

    return this.state.itemsLoaded ? (
      <Table>
        <TableHeader>
          <TableRow>
            <TableCell scope="col" border="bottom">
              Name
            </TableCell>
            <TableCell scope="col" border="bottom">
              Size
            </TableCell>
            <TableCell scope="col" border="bottom">
              Material
            </TableCell>
            <TableCell scope="col" border="bottom">
              Created
            </TableCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          { this.state.equipmentItems.map((item) => {
            return (
            <TableRow>
              <TableCell>
                {item.name}
              </TableCell>
            </TableRow>);
          })}
        </TableBody>
      </Table>
    ) : <div>Loading Items...</div>;
  }
}

const DesignHome = (props) => (
  <h2>Design Home</h2>
);

const Mash = (props) => (<h2>Mash</h2>);

export const DesignTab = ({ match }) =>
  (
    <div>
      <h1>Design</h1>
      <div>
        <Box direction="row">
          <Box direction="column" basis="small" background="brand">
            <Link to={`${match.url}/equipment`}><GrommetButton label="Equipment"/></Link>
            <Link to={`${match.url}/mash`}><GrommetButton label="Mash"/></Link>
          </Box>
          <Box basis="xlarge" background="neutral-2">
            <center>
              <Switch>
                <Route path={`${match.url}/home`} component={DesignHome} />
                <Route path={`${match.url}/equipment`} render={props => <EquipmentTable {...props} />} />
                <Route path={`${match.url}/mash`} component={Mash} />
                <Redirect from={match.url} to={`${match.url}/home`} />
              </Switch>
            </center>
          </Box>
        </Box>
      </div>
    </div>
);

