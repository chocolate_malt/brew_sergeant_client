import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, Switch } from "react-router-dom";
import { Box, Grommet, Button as GrommetButton, ThemeContext } from 'grommet';
import { normalizeColor} from "grommet/utils";
import { DesignTab } from './design-tab'

import './app.css';


const theme = {
  global: {
    colors: {
      'brand': '#721B00',
      'neutral-1': '#721B00',
      'neutral-2': '#5D0A02',
      'neutral-3': '#35090A',
      'neutral-4': '#060303',
      'accent-1': '#FDD978',
      'accent-2': '#F7B324',
      'accent-3': '#EF9001',
      'accent-4': '#DA8301'
    },
    edgeSize: {
      small: '14px'
    },
    elevation: {
      light: {
        medium: '0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)'
      },
    },
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
    },
  },
};

const NavBar = (props) => (
  <Box
  tag='header'
  direction='row'
  align='center'
  justify='between'
  background='light-2'
  pad={{ vertical: 'small', horizontal: 'medium' }}
  elevation='medium'
  {...props}
  />
);

const NavBarButton = (props) => (
  <ThemeContext.Extend
    value={{
      button: {
        border: {
          width: '0px',
          radius: '4px',
        },
        margin: 'large',
        extend: props => `
          color: ${normalizeColor(props.textColor, theme)};
        `,
      },

      global: {
        focus: {
          border: {
            color: normalizeColor("brand", theme)
          }
        }
      },
    }}
    >
  <GrommetButton {...props}/>
  </ThemeContext.Extend>
);

const NavBarColorPalleteExample = (props) => (
  <NavBar>
    <Box direction='row'>
      <NavBarButton primary="true" color="neutral-1" textColor="accent-1" label="neutral-1"/>
      <NavBarButton primary="true" color="neutral-2" textColor="accent-2" label="neutral-2"/>
      <NavBarButton primary="true" color="neutral-3" textColor="accent-3" label="neutral-3"/>
      <NavBarButton primary="true" color="neutral-4" textColor="accent-4" label="neutral-4"/>
      <NavBarButton primary="true" color="accent-1" textColor="neutral-1" label="accent-1"/>
      <NavBarButton primary="true" color="accent-2" textColor="neutral-2" label="accent-2"/>
      <NavBarButton primary="true" color="accent-3" textColor="neutral-3" label="accent-3"/>
      <NavBarButton primary="true" color="accent-4" textColor="neutral-4" label="accent-4"/>
    </Box>
  </NavBar>
);



const BrewTab = (props) => (
  <h1>Brew</h1>
);

const AccountTab = (props) => (
  <h1>Account</h1>
);

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentNav: "Design"
    };
  }

  handleNavClick(navItemName) {
    // return "foo";
    this.setState({currentNav: navItemName});
  }

  navIsActive(navItemName) {
    return this.state.currentNav === navItemName;
  }

  getNavLinkClassNames(navItemName) {
    return "nav-link" + (this.navIsActive(navItemName) ? " active" : "");
  }

  render() {
    return (
      <Grommet theme={theme}>
        <Router>
          <div>
            <NavBar>
              <Box direction="row">
                <Link to="/design"><NavBarButton color="neutral-1" textColor="neutral-1" label="Design"/></Link>
                <Link to="/brew"><NavBarButton color="neutral-1" textColor="neutral-1" label="Brew"/></Link>
              </Box>
              <Link to="/account"><NavBarButton color="neutral-1" textColor="neutral-1" label="Account" alignSelf="end"/></Link>
            </NavBar>
            <main>
              <Route path="/design" render={DesignTab} />
              <Route exact path="/brew" component={BrewTab} />
              <Route exact path="/account" component={AccountTab} />
            </main>
          </div>
        </Router>

      </Grommet>
    );
  }
}

export default App;
